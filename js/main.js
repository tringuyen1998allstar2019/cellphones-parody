var cart = [];
var listProducts = [];
const createListProducts = function(data) {
    var phone = "";
    for (var i = 0; i < data.length; i++) {
        phone += `  
        <div class="col-md-6 col-lg-4" style="width:50%">
        <div class="card shadow-sm border-light mb-4">
            <a href="#" class="position-relative text-center">
                <img src="${data[i].img}" class="mt-3" style="width:auto; height:200px;" alt="image"> </a>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 mb-3">
                        <a href="#" class="text-center">
                            <h4 class="font-weight-normal">${data[i].name.toUpperCase()}</h4>
                        </a>
                        <!-- <div class="d-flex justify-content-center my-4">
                            <h4>Samsung</h4>
                        </div> -->
                    </div>
                    <div class="col-12 text-center">
                        <div class="col"><span class="h5 text-danger font-weight-bold">${data[i].price}$</span></div>
                    </div>
                    <div class="col-12 text-center mb-3">
                        <i class="fa fa-star text-warning"></i>
                        <i class="fa fa-star text-warning"></i>
                        <i class="fa fa-star text-warning"></i>
                        <i class="fa fa-star text-warning"></i>
                        <i class="fa fa-star text-warning"></i>
                    </div>
                    <div class="col-12" style=" height:50px">
                        <div class="post-meta"><span class="small" style="">${data[i].desc}</span></div>
                    </div>
                </div>
                <div class=" row d-flex justify-content-between text-center">
                    <div class="col"><span class="text-muted font-small d-block mb-2">Camera</span></div>
                </div>
                <div class=" row d-flex justify-content-between  text-center">
                    <div class="col-12"> <span class="text-dark font-weight-bold" style="font-size:16px">Front: ${data[i].frontCamera} <br/> Back: ${data[i].backCamera}</span></div>
                    <button type="button" class="btn  w-100 mt-2" style="background:#009879" onclick="addToCard(${data[i].id})"><i class="fa fa-cart-plus"></i></button>
                    </div>

            </div>
        </div>
    </div>`
    }
    document.getElementById("ads").innerHTML = phone;
}
const fetchPhones = function() {
    axios({
        url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
        method: "GET"
    }).then(function(response) {
        console.log(response.data);
        createListProducts(response.data);
        listProducts = mapData(response.data);
        console.log(listProducts);
    }).catch(function(error) {
        console.log("Lỗi ở fetchPhones():", error);
    });
}
const filterListType = function() {
    var listFiltered = [];
    var value = document.getElementById("filterValue").value;
    console.log(value);
    axios({
        url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
        method: "GET"
    }).then(function(response) {
        if (value == "All") {
            createListProducts(response.data);
            return;
        };
        if (value == 1) {
            for (var i = 0; i < response.data.length; i++) {
                if (response.data[i].type == "samsung") {
                    listFiltered.push(response.data[i]);
                }
            }
            console.log(listFiltered);
            createListProducts(listFiltered);
            return;
        };
        if (value == 2) {
            for (var i = 0; i < response.data.length; i++) {
                if (response.data[i].type == "iphone") {
                    listFiltered.push(response.data[i]);
                }
            }
            createListProducts(listFiltered);
            return;
        };
    }).catch(function(error) {
        console.log("Lỗi ở filterListType():", error);
    });

}
const addToCard = function(id) {
    for (var i = 0; i < listProducts.length; i++) {
        if (listProducts[i].id == id) {
            if (listProducts[i].quantity == undefined) {
                listProducts[i].quantity = 1;
            }
            if (!listProducts[i].quantity != undefined) {
                for (var j = 0; j < cart.length; j++) {
                    if (cart[j].id == id) {
                        cart[j].quantity++;
                        console.log(cart);
                        renderCart();
                        return;
                    }
                }
            }
            cart.push(listProducts[i]);
        }
    }
    renderCart();
}
const mapData = function(dataFromDB) {
    var mappedData = [];
    for (var i = 0; i < dataFromDB.length; i++) {
        var id = dataFromDB[i].id;
        var name = dataFromDB[i].name;
        var price = dataFromDB[i].price;
        var screen = dataFromDB[i].screen
        var backCamera = dataFromDB[i].backCamera;
        var frontCamera = dataFromDB[i].frontCamera;
        var img = dataFromDB[i].img;
        var desc = dataFromDB[i].desc;
        var type = dataFromDB[i].type;
        var mappedProduct = new Product(id, name, price, screen, backCamera, frontCamera, img, desc, type);
        mappedData.push(mappedProduct);
    }
    return mappedData;
}
const renderCart = function() {
    var cartHTML = "";
    var total = 0;
    saveLocalStorage(cart);
    for (var i = 0; i < cart.length; i++) {
        total = total + cart[i].price * cart[i].quantity;
        cartHTML += `
        <tr class="">
        <td><img style="width:100px;" src="${cart[i].img}" /></td>
        <td class="font-weight-bold">${cart[i].name}</td>
        <td>${cart[i].price}</td>
        <td>
        <div class="btn-group">
        <button class="btn " style=" background-color: #009879" onclick="minusQuantity(${cart[i].id})">-</button>
        <input type="number" class="" value="${cart[i].quantity}" id="quantity" style="width:50px; padding-left: 20px" />
        <button class="btn" style=" background-color: #009879" onclick="addQuantity(${cart[i].id})">+</button>
        </div>
        </td>
        <td>${cart[i].price * cart[i].quantity}$</td>
        <td>
            <button style="width:40px; height:40px" class="btn btn-danger rounded-circle" onclick="removeCart(${cart[i].id})">
                  <i class="fa fa-trash"></i>
              </button>
        </td>
    </tr>
         `

    }
    console.log(total);
    document.getElementById("cartAdded").innerHTML = cartHTML;
    document.getElementById("totalPrice").innerHTML = total;

}
const minusQuantity = function(id) {
    for (var i = 0; i < cart.length; i++) {
        if (cart[i].id == id) {
            cart[i].quantity--;
            renderCart();
            return;
        }
    }

}
const addQuantity = function(id) {
    for (var i = 0; i < cart.length; i++) {
        if (cart[i].id == id) {
            cart[i].quantity++;
            renderCart();
            return;
        }
    }

}
const removeCart = function(id) {
    for (var i = 0; i < cart.length; i++) {
        if (cart[i].id == id) {
            cart.splice(i, 1);
        }
    }
    renderCart();
}
const saveLocalStorage = function(cartList) {
    //hàm chuyển từ object => json
    const cartListJSON = JSON.stringify(cartList);
    console.log(cartListJSON);
    localStorage.setItem("data", cartListJSON);
};
const getDataFromLocalStorage = function() {
    const dataJSON = localStorage.getItem("data");

    //kiểm tra data có tồn tại, ko thì return
    if (!dataJSON) return;
    //chuyển từ json => object
    const data = JSON.parse(dataJSON);
    //gán data lấy đc vào list student
    cart = data;

};
const checkout = function() {
    cart = [];
    renderCart();
    alert("Check out success! Thanks you very much");
}

fetchPhones();
getDataFromLocalStorage();
renderCart();