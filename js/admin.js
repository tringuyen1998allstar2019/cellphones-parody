var listProducts = [];
const createListProducts = function(data) {
    var phone = "";
    for (var i = 0; i < data.length; i++) {
        phone += `  
        <tr class="">
        <td>${data[i].id}</td>
        <td><img style="width:100px;" src="${data[i].img}" /></td>
        <td class="font-weight-bold">${data[i].name}</td>
        <td>${data[i].price}</td>
        <td>${data[i].screen}</td>
        <td>${data[i].frontCamera}</td>
        <td>${data[i].backCamera}</td>
        <td>${data[i].desc}</td>
        <td>${data[i].type}</td>
        <td>
            <button style="width:40px; height:40px" class="btn btn-danger rounded-circle">
                  <i class="fa fa-trash"></i>
              </button>
            <button style="width:40px; height:40px" class="btn btn-primary rounded-circle">
                <i class="fa fa-link"></i>
            </button>
        </td>
    </tr>
       `
    }
    document.getElementById("cartAdded").innerHTML = phone;
}
const fetchPhones = function() {
    axios({
        url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
        method: "GET"
    }).then(function(response) {
        console.log(response.data);
        var mappedData = mapData(response.data);
        listProducts = mappedData;
        createListProducts(response.data);
    }).catch(function(error) {
        console.log("Lỗi ở fetchPhones():", error);
    });
}
const searchPhones = function() {
    listSearchPhones = [];
    var keyword = document.getElementById("searhValue").value;
    keyword = keyword.toLowerCase().trim();
    for (var i = 0; i < listProducts.length; i++) {
        var convertedFullName = listProducts[i].name.toLowerCase();
        if (listProducts[i].id == keyword || convertedFullName.includes(keyword)) {
            listSearchPhones.push(listProducts[i]);
        }
    }
    console.log(listSearchPhones);
    createListProducts(listSearchPhones);
}
const mapData = function(dataFromDB) {
    var mappedData = [];
    for (var i = 0; i < dataFromDB.length; i++) {
        var id = dataFromDB[i].id;
        var name = dataFromDB[i].name;
        var price = dataFromDB[i].price;
        var screen = dataFromDB[i].screen
        var backCamera = dataFromDB[i].backCamera;
        var frontCamera = dataFromDB[i].frontCamera;
        var img = dataFromDB[i].img;
        var desc = dataFromDB[i].desc;
        var type = dataFromDB[i].type;
        var mappedProduct = new Product(id, name, price, screen, backCamera, frontCamera, img, desc, type);
        mappedData.push(mappedProduct);
    }
    return mappedData;
}

fetchPhones();